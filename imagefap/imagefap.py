import argparse
import requests
import re
import os

from tqdm import tqdm

TIMEOUT = 10  # 10 seconds
CACHE = '.cache'
CACHE_FAILED_REQUESTS = '.cache.failed'

parser = argparse.ArgumentParser()
parser.add_argument("url", help='Gallery or Photo URL')
parser.add_argument("--path", default='downloads', help='directory to download images to')
parser.add_argument("--retry", default=False, help='retry downloading failed images (experimental)')
args, unknown = parser.parse_known_args()


def cache_source(src):
  with open(CACHE) as f:
    f.write(requests.get(src).text)


def read_cache():
  with open(CACHE) as f:
    return f.read()


def cache_failed_request(url):
  with open(CACHE_FAILED_REQUESTS) as f:
    f.write(url)
    f.write('\n')


def retry_failed_requests():
  [ download_with_requests(url, path=args.path) for url in
      open(CACHE_FAILED_REQUESTS).readlines()
      if url
    ]


def download_with_requests(url, path='.'):
  local_filename = url.split('/')[-1]
  # NOTE the stream=True parameter
  try:
    r = requests.get(url, stream=True, timeout=TIMEOUT)
    with open(os.path.join(path, local_filename), 'wb') as f:
      for chunk in r.iter_content(chunk_size=1024):
        if chunk:  # filter out keep-alive new chunks
          f.write(chunk)
          # f.flush() commented by recommendation from J.F.Sebastian
  except requests.exceptions.Timeout:
    print('Request Timed Out for ', url)
  return local_filename


def get_title(gallery_url):
  src_text = requests.get(gallery_url).text
  title_tag = re.findall(r'<title>.*?</title>', src_text)[0]
  title = title_tag[7:-7 - 1].replace('/', '_')
  return title[:title.find(' Porn Pics &amp; Porn GIFs')]


def download_images(url, path='.', page=0):
  # check if url exists
  if url is None:
    print('No images at ', url)

  if not os.path.exists(path):
    os.mkdir(path)

  # page_url = url + '/?&page={}'.format(page)
  page_url = url
  print('Downloading images from [{remote}] -> [{local}]'.format(remote=page_url, local=path))

  src_text = requests.get(page_url).text
  images = [ item[item.find('"') + 1: -1]
      for item in re.findall(r'original=".*?"', src_text) ]
  assert len(images) > 0

  [ download_with_requests(im, path=path) for im in tqdm(images) ]

  # check if number of images in gallery is 24
  if len(images) == 24:
    print('Moving on to page ', page + 2)
    # download images from next page
    download_images(
        gallery_to_photo_url(
          photo_to_gallery_url(url) + '&page={}'.format(page + 1)
        ),
        path=path,
        page=page + 1
    )

  return images


def gallery_to_photo_url(url):
  src_text = requests.get(url).text
  photo_ids = re.findall(r'href="\/photo\/.*?"', src_text)
  if len(photo_ids):
    photo_id = '/'.join(photo_ids[0][6:].split('/')[:3])
    return "https://www.imagefap.com" + photo_id

  return None


def photo_to_gallery_url(url):
  # check if it's gallery url
  if url and re.match(r'https\:\/\/www.imagefap.com\/gallery.php\?gid\=.*?', url):
    return url

  src_text = requests.get(url).text
  gallery_url = re.findall(
      r'href="https\:\/\/www.imagefap.com\/gallery.php\?gid\=.*?"',
      src_text)[0]
  print(url, '-->', gallery_url[6:-1])
  return gallery_url[6:-1]


if __name__ == '__main__':

  url = args.url
  if url and re.match(r'https\:\/\/www.imagefap.com\/gallery.php\?gid\=.*?', url):
     url = gallery_to_photo_url(url)

  if url and re.match(r'https\:\/\/www.imagefap.com\/gallery\/.*?', url):
    url = gallery_to_photo_url(url)

  if url and re.match(r'https\:\/\/www.imagefap.com\/pictures\/.*?\/.*?', url):
    url = gallery_to_photo_url(url)

  if url and re.match(r'https\:\/\/www.imagefap.com\/photo\/.*?', url):
    # get title
    title = get_title(photo_to_gallery_url(url))
    # download images
    download_images(url, path=os.path.join(args.path, title))
