# imagefap

command-line tool to download images from [imagefap](https://www.imagefap.com)


## usage

```bash
python3 imagefap.py <URL>
```
